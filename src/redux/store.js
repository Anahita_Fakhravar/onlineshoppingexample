import {createStore,applyMiddleware} from 'redux';
import logger from 'redux-logger';
import rootReducer from './root-reducer';
//This middleware is between action and reducer and
// jsut log information fllowing between the two parts

const middlewares = [logger];
const store = createStore(rootReducer,applyMiddleware(...middlewares))

export default store;