import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config =
{
  apiKey: "AIzaSyDh1iimG2Qv3mup1dMnUrQQNyxcAks_DbE",
  authDomain: "online-shopping-store-react-db.firebaseapp.com",
  databaseURL: "https://online-shopping-store-react-db.firebaseio.com",
  projectId: "online-shopping-store-react-db",
  storageBucket: "online-shopping-store-react-db.appspot.com",
  messagingSenderId: "1043663412544",
  appId: "1:1043663412544:web:95561841007aaa31046ea5",
  measurementId: "G-21SYYF3K19"
}


firebase.initializeApp(config)

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;
  const userRef = firestore.doc(`users/${userAuth.uid}`);
  const snapShot = await userRef.get();
  console.log('user68', userRef)

  if (!snapShot.exists) {
    console.log('annnnnnnnna login 3')
    const { displayName, email } = userAuth;
    const createdAt = new Date();

    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      })
    } catch (error) {
      console.log('error to create user', error.message)

    }
  }

  return userRef;

}

  export default firebase;