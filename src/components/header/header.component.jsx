//Header design page 

import React from 'react';
import { Link } from 'react-router-dom';
import './header.styles.scss';
import { ReactComponent as Logo } from '../../assets/crown1.svg';
import { auth } from '../../firebase/firebas.utiles';
import { connect } from 'react-redux';
import CartIcon from '../cart-icon/cart-icon.component';
import CartDropdown from '../cart-dropdown/cart-dropdown.component';


const Header = ({ currentUser, hidden }) => {
    return (
        <div className='header'>

            <Link className='logo-container' to='/'>
                <Logo className='logo' />
            </Link>

            <div className='options'>
                <Link className='option' to='/shop'>SHOP</Link>
                <Link className='option' to='/shop'>CONTACT</Link>

                {currentUser ? <div className='option'
                    onClick={() => auth.signOut()}>SIGN OUT</div>
                    : <Link className='option' to='/signin'>SIGN IN</Link>}

                <CartIcon />
            </div>

            { hidden ? null :

                <CartDropdown />}

        </div>
    )
}
//More advance way of getting state 

const mapStateToProps = ({ user: { currentUser }, cart: { hidden } }) => ({
    currentUser,
    hidden
})

/* const mapStateToProps = state => ({
    currentUser: state.user.currentUser
}) */
export default connect(mapStateToProps)(Header);  