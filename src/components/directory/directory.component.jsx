import React from 'react';
import MenuItem from './../menu-items/menu-item.component';
import './directory.styles.scss';

class Directory extends React.Component {

    constructor() {
        super();
        this.state = {
            sections: [
                {
                    title: 'Hats',
                    imageUrl: 'https://images-na.ssl-images-amazon.com/images/I/61-E13qpNcL._AC_UX569_.jpg',
                    id: 1,
                    linkUrl:'hats',
                },
                {
                    title: 'Jackets',
                    imageUrl: 'https://images-na.ssl-images-amazon.com/images/I/61ceoIyhnvL._AC_UX679_.jpg',
                    id: 2,
                    linkUrl:'',
                },
                {
                    title: 'Sneakers',
                    imageUrl: 'https://images-na.ssl-images-amazon.com/images/I/71MK7pKW4vL._AC_UX575_.jpg',
                    id: 3,
                    linkUrl:'',
                },
                {
                    title: 'Womens',
                    imageUrl: 'https://img.freepik.com/free-photo/girl-yellow-wall-with-shopping-bags_1157-34344.jpg?size=626&ext=jpg',
                    size: 'large',
                    id: 4,
                    linkUrl:'',
                },
                {
                    title: 'Mens',
                    imageUrl: 'https://www.contemporist.com/wp-content/uploads/2017/02/modern-mens-fashion-leather-090217-429-04-800x534.jpg',
                    size: 'large',
                    id: 4,
                    linkUrl:'',
                },
            ]


        }
    }

    render() {
        return (
            <dive className='directory-menu'>
                {this.state.sections.map(({ id,...otherSectionProps}) =>
                    <MenuItem key={id} {...otherSectionProps} />
                )}
            </dive>
        )
    }
}

export default Directory