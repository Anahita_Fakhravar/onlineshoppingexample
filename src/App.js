import React from 'react';
import './App.css';
import HomePage from './pages/homepage/homepage.component';
import { Route, Switch, Redirect } from 'react-router-dom';
import ShopPage from './pages/shop/shop.component';
import Header from './components/header/header.component'
import SignInAndSignUpPage from '../src/pages/sign-in-and-sign-up/sign-in-and-sign-up.component'
import { auth, createUserProfileDocument } from './firebase/firebas.utiles';
import { setCurrentUser } from './redux/user/user.actions';
import { connect } from 'react-redux'

class App extends React.Component {

  /* constructor() {
    super();
    this.state = {
      currentUser: null
    }
  } */

  unsubscribeFromAuth = null;

  componentDidMount() {

    const { setCurrentUser } = this.props;

    console.log('annnnnnnnna login')
    this.unsubscribeFromAuth = auth.onAuthStateChanged(async userAuth => {
      console.log('annnnnnnnna login1')
      // this.setState({ currentUser: user })
      //  console.log('user',user)
      if (userAuth) {
        const userRef = await createUserProfileDocument(userAuth)
        userRef.onSnapshot(snapShot => {
          this.props.setCurrentUser({
            id: snapShot.id,
            ...snapShot.data()
          })
          console.log('ana log', this.state)
        })

      }

      setCurrentUser(userAuth);

    })
  }

  componentWillUnmount() {
    this.unsubscribeFromAuth()
  }

  render() {
    return (

      <dive>
        <Header />
        <Switch>
          <Route exact path={'/'} component={HomePage} />
          <Route path={'/shop'} component={ShopPage} />
          <Route exact path={'/signin'}
            render={() => this.props.currentUser ?
              (<Redirect />)
              : (<SignInAndSignUpPage />)} />
        </Switch>
      </dive>

    );
  }

}
const mapStateToProps = ({ user }) => ({
  currentUser: user.currentUser
})
const mapDispatchToProps = dispatch => ({

  setCurrentUser: user => dispatch(setCurrentUser(user))
})
export default connect(mapStateToProps, mapDispatchToProps)(App);
